# Team 7 Smart Garage Project

Trello board : https://trello.com/b/izAMhkNv/smart-garage

To create database: Open a query console and run generateDatabase.sql contained in the "db" folder.

To insert data into the database: run insertion_script.

Swagger Documentation: http://localhost:8080/swagger-ui.html#/

# Building and running the project

You need Java 1.11 and a bash-like shell.

Invoke the build at the root of the project: 
```$ ./gradlew build```

Invoke the run command: 
```$ ./gradlew bootRun```
