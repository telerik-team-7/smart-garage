package com.team7.garage.utils;

import com.team7.garage.contracts.CarRepository;
import com.team7.garage.contracts.ManufacturerRepository;
import com.team7.garage.contracts.ModelRepository;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.InvalidDateException;
import com.team7.garage.exceptions.InvalidRegistrationNumberException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.models.*;

import java.time.LocalDate;
import java.util.List;

public class Validator {

    private static final String ONLY_EMPLOYEES_CAN_ACCESS_THIS_METHOD = "Only employees can access this method";
    private static final String INVALID_REGION_CODE = "Invalid region code";
    private static final String INVALID_REGISTRATION_NUMBER = "Invalid registration number";
    private static final String INVALID_DATE = "End date must be after start date.";

    public static <T> void ValidateListNotEmpty(List<T> list, String type) {
        if (list.size() == 0) {
            throw new EntityNotFoundException(type);
        }
    }

    public static void ValidateAuthorization(User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(ONLY_EMPLOYEES_CAN_ACCESS_THIS_METHOD);
        }
    }

    public static void ValidateAccess(User user, User caller) {
        if (!caller.isAdmin() && !user.getEmail().equals(caller.getEmail())) {
            throw new UnauthorizedOperationException(ONLY_EMPLOYEES_CAN_ACCESS_THIS_METHOD);
        }
    }

    public static void ValidateRegistrationNumber(String registrationNumber) {
        List<String> regions = List.of("A", "B", "BH", "BP", "BT", "E", "EB", "EH", "K", "KH", "M", "H", "OB", "P",
                "PA", "PB", "PK", "PP", "C", "CA", "CB", "CH", "CM", "CO", "CC", "CT", "T", "TX", "Y", "X");

        String regionNumber;
        String numerals;
        String series;

        if (registrationNumber.length() == 8) {
            regionNumber = registrationNumber.substring(0, 2);
            numerals = registrationNumber.substring(2, 6);
            series = registrationNumber.substring(6);
        } else {
            regionNumber = registrationNumber.substring(0, 1);
            numerals = registrationNumber.substring(1, 5);
            series = registrationNumber.substring(5);
        }

        if (!regions.contains(regionNumber)) {
            throw new InvalidRegistrationNumberException(INVALID_REGION_CODE);
        }

        for (Character c : numerals.toCharArray()) {
            if (!Character.isDigit(c)) {
                throw new InvalidRegistrationNumberException(INVALID_REGISTRATION_NUMBER);
            }
        }

        for (Character c : series.toCharArray()) {
            if (Character.isDigit(c)) {
                throw new InvalidRegistrationNumberException(INVALID_REGISTRATION_NUMBER);
            }
        }
    }

    public static void ValidateDates(Visit visit) {
        if (!visit.getStartDate().isBefore(visit.getEndDate())) {
            throw new InvalidDateException(INVALID_DATE);
        }
    }

    public static void ValidateUsersSize(List<User> users, String attribute, String value){
        if (users.size() == 0){
            throw new EntityNotFoundException("Users", attribute, value);
        }
    }

    public static void ValidateUsersSize(List<User> users) {
        if (users.size() == 0){
            throw new EntityNotFoundException("Users");
        }
    }
}
