package com.team7.garage.utils;

import com.team7.garage.models.Procedure;
import com.team7.garage.models.User;
import com.team7.garage.models.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
public class EmailSender {

    private static final String SENDER = "smart.garage.team7@gmail.com";
    @Autowired
    private JavaMailSender mailSender;

    public void sendSimpleEmail(String toEmail,
                                String body,
                                String subject) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(SENDER);
        message.setTo(toEmail);
        message.setText(body);
        message.setSubject(subject);

        mailSender.send(message);
    }

    public String extractBody(Visit visit) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(String.format("Hello, %s%n%n" +
                        "A visit has been registered under your name,%n" +
                        "Car: %s with registration number: %s%n" +
                        "Procedures: ",
                visit.getCar().getUser().getFirstName(),
                visit.getCar().getModel().getManufacturer().getName() + " " +
                        visit.getCar().getModel().getName(),
                visit.getCar().getRegNumber()));

        for (Procedure procedure :
                visit.getProcedures()) {
            stringBuilder.append(procedure.getName()).append(", Price: ")
                    .append(procedure.getPrice()).append(" BGN").append(System.lineSeparator());
        }

        stringBuilder.append("Total Price: ").append(visit.calculateTotalPrice(1.0))
                .append(" BGN").append(System.lineSeparator());

        stringBuilder.append(System.lineSeparator()).append("Thank you for using our services!");

        return stringBuilder.toString();
    }

    public void SendPassword(User user) {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setFrom("contact@smart-garage.com", "Smart Garage Support");
            helper.setTo(user.getEmail());
            String subject = "Your password";
            String content = "<p>Hello,</p>"
                    + "<p>You have made a new account and this will be your new password: </p>"
                    + "<p>" + user.getPassword() + "</p>";
            helper.setSubject(subject);
            helper.setText(content, true);
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        mailSender.send(message);
    }
}
