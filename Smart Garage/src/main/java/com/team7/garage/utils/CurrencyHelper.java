package com.team7.garage.utils;

import com.team7.garage.exceptions.InvalidCurrencyException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class CurrencyHelper {

    private static final String INVALID_CURRENCY = "Invalid Currency";


    public static Double getRate(String currency) {

        if (currency == null) {
            return 1.0;
        }

        String backUpKey = "604ea0df30aea2d924e2";

        String uri = String.format(
                "https://free.currconv.com/api/v7/convert?q=BGN_%s&compact=ultra&apiKey=c1cc5b5425cd8b9a7062"
                ,currency);
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        if (result == null) {
            throw new InvalidCurrencyException(INVALID_CURRENCY);
        }

        List<Character> chars = new ArrayList<>();
        for (char c :
                result.toCharArray()) {
            chars.add(c);
        }

        for (int i = 0; i < chars.size(); i++) {
            if (!Character.isDigit(chars.get(i)) && !Character.toString(chars.get(i)).equals(".")) {
                chars.remove(i);
                i--;
            }
        }

        if (chars.size() == 0) {
            throw new InvalidCurrencyException(INVALID_CURRENCY);
        }

        StringBuilder sb = new StringBuilder();

        for (Character ch :
                chars) {
            sb.append(ch);
        }

        String rateAsString = sb.toString();

        return Double.parseDouble(rateAsString);
    }
}
