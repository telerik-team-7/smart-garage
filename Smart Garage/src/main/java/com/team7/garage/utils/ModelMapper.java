package com.team7.garage.utils;

import com.team7.garage.contracts.*;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ModelMapper {

    private final CarRepository carRepository;

    private final CarService carService;
    private final ProcedureRepository procedureRepository;

    private final VisitRepository visitRepository;
    private final ModelRepository modelRepository;
    private final ManufacturerRepository manufacturerRepository;

    private final AuthenticationHelper authenticationHelper;
    private final UserRepository userRepository;

    private final UserService userService;
    private final RoleRepository roleRepository;

    @Autowired
    public ModelMapper(CarRepository carRepository,
                       CarService carService,
                       ProcedureRepository procedureRepository,
                       VisitRepository visitRepository,
                       ModelRepository modelRepository,
                       ManufacturerRepository manufacturerRepository,
                       AuthenticationHelper authenticationHelper,
                       UserRepository userRepository,
                       UserService userService,
                       RoleRepository roleRepository) {
        this.carRepository = carRepository;
        this.carService = carService;
        this.procedureRepository = procedureRepository;
        this.visitRepository = visitRepository;
        this.modelRepository = modelRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.authenticationHelper = authenticationHelper;
        this.userRepository = userRepository;
        this.userService = userService;
        this.roleRepository = roleRepository;
    }

    public Car fromDto(CarDto carDto) {
        Car car = new Car();
        dtoToObject(carDto, car);
        return car;
    }

    public Car fromDto(CarDto carDto, int id) {
        Car car = carRepository.getById(id);
        dtoToObject(carDto, car);
        return car;
    }

    private Car dtoToObject(CarDto carDto, Car car) {
        Manufacturer manufacturer = manufacturerRepository.getByNameOrCreate(carDto.getManufacturer());
        Model model = modelRepository.getByNameOrCreate(carDto.getModel(), manufacturer);
        User user = userRepository.getById(carDto.getUserId());

        car.setVin(carDto.getVin());
        Validator.ValidateRegistrationNumber(carDto.getRegNumber().toUpperCase());
        car.setRegNumber(carDto.getRegNumber().toUpperCase());
        car.setModel(modelRepository.getByName(model.getName()));
        car.setUser(user);

        return car;
    }

    public CarDto toDto(Car car) {
        CarDto carDto = new CarDto();

        carDto.setVin(car.getVin());
        carDto.setRegNumber(car.getRegNumber());
        carDto.setModel(car.getModel().getName());
        carDto.setManufacturer(car.getModel().getManufacturer().getName());
        carDto.setUserId(car.getUser().getId());

        return carDto;
    }

    public Procedure fromDto(ProcedureDto procedureDto) {
        Procedure procedure = new Procedure();
        dtoToObject(procedureDto, procedure);
        return procedure;
    }

    public Procedure fromDto(ProcedureDto procedureDto, int id) {
        Procedure procedure = procedureRepository.getById(id);
        dtoToObject(procedureDto, procedure);
        return procedure;
    }

    private Procedure dtoToObject(ProcedureDto procedureDto, Procedure procedure) {
        procedure.setName(procedureDto.getName());
        procedure.setPrice(procedureDto.getPrice());

        return procedure;
    }

    public ProcedureDto toDto(Procedure procedure) {
        ProcedureDto procedureDto = new ProcedureDto();

        procedureDto.setName(procedure.getName());
        procedureDto.setPrice(procedure.getPrice());

        return procedureDto;
    }

    public Visit fromDto(VisitDto visitDto) {
        Visit visit = new Visit();
        dtoToObject(visitDto, visit);
        return visit;
    }

    public Visit fromDto(VisitDto visitDto, int id) {
        Visit visit = visitRepository.getById(id);
        dtoToObject(visitDto, visit);
        return visit;
    }

    private Visit dtoToObject(VisitDto visitDto, Visit visit) {
        Car car = carRepository.getById(visitDto.getCarId());
        Set<Procedure> procedures = new HashSet<>();
        for (String procedure :visitDto.getProcedures()) {
            procedures.add(procedureRepository.getByName(procedure));
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        visit.setCar(car);
        visit.setProcedures(procedures);
        visit.setEndDate(LocalDate.parse(visitDto.getEndDate(), formatter));

        return visit;
    }

    public VisitDto toDto(VisitResponseDto visitResponseDto) {
        VisitDto visitDto = new VisitDto();

        visitDto.setCarId(visitResponseDto.getCar().getId());
        visitDto.setProcedures(visitResponseDto.getProcedures()
                .stream()
                .map(Procedure::getName).collect(Collectors.toSet()));
        visitDto.setEndDate(visitResponseDto.getEndDate().toString());

        return visitDto;
    }

    public Visit fromDto(NewUserVisitDto newUserVisitDto) {
        return dtoToObject(newUserVisitDto);
    }

    private Visit dtoToObject(NewUserVisitDto newUserVisitDto) {
        User user = fromDto(newUserVisitDto.getUserDto());
        userService.create(user);

        CarDto carDto = new CarDto();
        carDto.setManufacturer(newUserVisitDto.getManufacturer());
        carDto.setModel(newUserVisitDto.getModel());
        carDto.setRegNumber(newUserVisitDto.getRegNumber());
        carDto.setVin(newUserVisitDto.getVin());
        carDto.setUserId(user.getId());

        Car car = fromDto(carDto);
        try {
            carService.create(car, authenticationHelper.getAdmin());
        } catch (UnauthorizedOperationException e) {
            userRepository.delete(user.getId());
            throw new UnauthorizedOperationException(e.getMessage());
        } catch (DuplicateEntityException e) {
            userRepository.delete(user.getId());
            throw new DuplicateEntityException(e.getMessage());
        }

        VisitDto visitDto = new VisitDto();
        visitDto.setCarId(car.getId());
        visitDto.setEndDate(newUserVisitDto.getEndDate().toString());
        visitDto.setProcedures(newUserVisitDto.getProcedures());

        return fromDto(visitDto);
    }

    public VisitResponseDto toResponseDto(Visit visit, String currency) {
        VisitResponseDto visitResponseDto = new VisitResponseDto();
        toResponseDto(visit, visitResponseDto, currency);
        return visitResponseDto;
    }

    private VisitResponseDto toResponseDto(Visit visit, VisitResponseDto visitResponseDto, String currency) {
        visitResponseDto.setId(visit.getId());
        visitResponseDto.setCar(visit.getCar());
        visitResponseDto.setStartDate(visit.getStartDate());
        visitResponseDto.setEndDate(visit.getEndDate());
        visitResponseDto.setProcedures(visit.getProcedures());
        Double rate = CurrencyHelper.getRate(currency);

        if (currency == null) {
            visitResponseDto.setPrice(visit.calculateTotalPrice(rate) + " BGN");
        } else {
            visitResponseDto.setPrice(visit.calculateTotalPrice(rate) + " " + currency.toUpperCase());
        }

        return visitResponseDto;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDtoAdmin(UserDto userDto) {
        User user = new User();
        dtoToObjectAdmin(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();

        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPhone(user.getPhone());
        //userDto.setRoles(user.getRoles());

        return userDto;
    }

    private void dtoToObject(UserDto userDto, User user) {
        Role role = roleRepository.getByName("User");
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        Set<Car> cars = new HashSet<>();

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        //user.setPassword(userDto.getPassword());
        user.setRoles(roles);
        user.setCars(cars);
    }

    private void dtoToObjectAdmin(UserDto userDto, User user) {
        Role role = roleRepository.getByName("User");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        role = roleRepository.getByName("Admin");
        roles.add(role);

        Set<Car> cars = new HashSet<>();

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        user.setRoles(roles);
        //user.setPassword(userDto.getPassword());
        user.setRoles(roles);
        user.setCars(cars);
    }
}


