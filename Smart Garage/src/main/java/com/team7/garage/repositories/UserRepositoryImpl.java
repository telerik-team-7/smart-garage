package com.team7.garage.repositories;

import com.team7.garage.contracts.UserRepository;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.Car;
import com.team7.garage.models.User;
import com.team7.garage.utils.Validator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User", User.class
            );
            List<User> users = query.list();
            Validator.ValidateUsersSize(users);
            return users;
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }



    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where email = :email", User.class
            );
            query.setParameter("email", email);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "e-mail", email);
            }
            return result.get(0);
        }
    }

    @Override
    public List<User> search(String email,
                             String firstName,
                             String lastName,
                             String phone) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where (email like :email or :email = '%null%')" +
                            " and (firstName like :firstName or :firstName = '%null%')" +
                            " and (lastName like :lastName or :lastName = '%null%')" +
                            " and (phone like :phone or :phone = '%null%')" , User.class
            );
            query.setParameter("email", "%" + email + "%");
            query.setParameter("firstName", "%" + firstName + "%");
            query.setParameter("lastName", "%" + lastName + "%");
            query.setParameter("phone", "%" + phone + "%");

            List<User> users = query.list();

            Validator.ValidateUsersSize(users);

            return users;
        }
    }

    @Override
    public User getByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where password_reset_token = :token", User.class
            );
            query.setParameter("token", token);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "reset token", token);
            }
            return result.get(0);
        }
    }
    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }



    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }



}
