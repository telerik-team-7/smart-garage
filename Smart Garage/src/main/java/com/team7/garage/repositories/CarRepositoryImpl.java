package com.team7.garage.repositories;

import com.team7.garage.contracts.CarRepository;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.Car;
import com.team7.garage.models.User;
import com.team7.garage.utils.Validator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {

    private static final String CAR = "Car";

    private static final String REGISTRATION = "registration number";

    private final SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Car> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(
                    "from Car", Car.class
            );
            return query.list();
        }
    }

    @Override
    public Car getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Car car = session.get(Car.class, id);
            if (car == null) {
                throw new EntityNotFoundException(CAR, id);
            }
            return car;
        }
    }

    @Override
    public List<Car> filter(String vin,
                               String regNumber,
                               String model,
                               String manufacturer,
                               String userFirstName,
                               String userLastName,
                               String userEmail) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(
                    "from Car where (vin = :vin or :vin is null)" +
                            " and (regNumber = :regNumber or :regNumber is null)" +
                            " and (model.name like :model or :model = '%null%')" +
                            " and (model.manufacturer.name like :manufacturer or :manufacturer = '%null%')" +
                            " and (user.firstName like :userFirstName or :userFirstName = '%null%')" +
                            " and (user.lastName like :userLastName or :userLastName = '%null%')" +
                            " and (user.email like :userEmail or :userEmail = '%null%')", Car.class
            );
            query.setParameter("vin", vin);
            query.setParameter("regNumber", regNumber);
            query.setParameter("model", "%" + model + "%");
            query.setParameter("manufacturer", "%" + manufacturer + "%");
            query.setParameter("userFirstName", "%" + userFirstName + "%");
            query.setParameter("userLastName", "%" + userLastName + "%");
            query.setParameter("userEmail", "%" + userEmail + "%");

            return query.list();
        }
    }

    @Override
    public Car getByRegistration(String registration) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(
                    "from Car where regNumber = :regNumber", Car.class
            );
            query.setParameter("regNumber", registration);
            List<Car> cars = query.list();
            Validator.ValidateListNotEmpty(cars, CAR);
            return cars.get(0);
        }
    }

    @Override
    public List<Car> getByModel(String model) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(
                    "from Car where model.name = :model", Car.class
            );
            query.setParameter("model", model);
            return query.list();
        }
    }

    @Override
    public List<Car> getByManufacturer(String manufacturer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(
                    "from Car where model.manufacturer.name = :manufacturer", Car.class
            );
            query.setParameter("manufacturer", manufacturer);
            return query.list();
        }
    }


    @Override
    public Car getByVIN(String vin) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(
                    "from Car where vin = :vin", Car.class
            );
            query.setParameter("vin", vin);
            List<Car> cars = query.list();
            Validator.ValidateListNotEmpty(cars, CAR);
            return cars.get(0);
        }
    }

    @Override
    public void create(Car car) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(car);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Car car) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(car);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Car carToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(carToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Car> getUserCars(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(
                    "from Car where user_id = :id", Car.class
            );
            query.setParameter("id", user.getId());
            List<Car> result = query.list();
           /* if (result.size() == 0) {
                throw new EntityNotFoundException("User", "id", Integer.toString(user.getId()));
            }*/
            return result;
        }
    }

}
