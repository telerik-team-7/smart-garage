package com.team7.garage.repositories;

import com.team7.garage.contracts.ProcedureRepository;
import com.team7.garage.contracts.VisitRepository;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.Visit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class VisitRepositoryImpl implements VisitRepository {

    private static final String VISIT = "Visit";
    private final SessionFactory sessionFactory;

    private final ProcedureRepository procedureRepository;


    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory,
                               ProcedureRepository procedureRepository) {
        this.sessionFactory = sessionFactory;
        this.procedureRepository = procedureRepository;
    }

    @Override
    public List<Visit> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                    "from Visit", Visit.class
            );
            return query.list();
        }
    }

    @Override
    public List<Visit> getByCarId(int carId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                    "from Visit where car.id = :carId", Visit.class
            );
            query.setParameter("carId", carId);
            return query.list();
        }
    }

    @Override
    public List<Visit> getByProcedureId(int procedureId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                    "from Visit", Visit.class
            );

            List<Visit> visits = query.list();
            visits = visits.stream().filter(visit -> visit.getProcedures().
                    contains(procedureRepository.getById(procedureId))).collect(Collectors.toList());
            return visits;
        }
    }

    @Override
    public List<Visit> getByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                    "from Visit where car.user.id = :userId", Visit.class
            );
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public Visit getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Visit visit = session.get(Visit.class, id);
            if (visit == null) {
                throw new EntityNotFoundException(VISIT, id);
            }
            return visit;
        }
    }

    @Override
    public void create(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Visit visitToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(visitToDelete);
            session.getTransaction().commit();
        }
    }

}
