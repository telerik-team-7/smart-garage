package com.team7.garage.repositories;

import com.team7.garage.contracts.ProcedureRepository;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.Procedure;
import com.team7.garage.models.User;
import com.team7.garage.models.Visit;
import com.team7.garage.utils.Validator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class ProcedureRepositoryImpl implements ProcedureRepository {

    private static final String PROCEDURE = "Procedure";

    private static final String NAME = "name";

    private final SessionFactory sessionFactory;

    @Autowired
    public ProcedureRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Procedure> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Procedure> query = session.createQuery(
                    "from Procedure", Procedure.class
            );
            return query.list();
        }
    }

    @Override
    public List<Procedure> filter(String name, Double price, Double minPrice, Double maxPrice) {
        try (Session session = sessionFactory.openSession()) {
            Query<Procedure> query = session.createQuery(
                    "from Procedure where (name like :name or :name = '%null%')" +
                            " and (price = :price or :price is null)" +
                            " and (price >= :minPrice or :minPrice is null)" +
                            " and (price <= :maxPrice or :maxPrice is null)", Procedure.class
            );
            query.setParameter("name","%" + name + "%");
            query.setParameter("price", price);
            query.setParameter("minPrice", minPrice);
            query.setParameter("maxPrice", maxPrice);

            var list = query.list();
            return list;
        }

    }

    @Override
    public List<Procedure> getForUser(User user, Integer carId, LocalDate date, LocalDate beforeDate, LocalDate afterDate) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                    "from Visit where (car.user.email = :email)" +
                            " and (car.id = :carId or :carId is null)" +
                            " and (startDate = :date or :date is null)" +
                            " and (endDate <= :beforeDate or :beforeDate is null)" +
                            " and (startDate >= :afterDate or :afterDate is null)", Visit.class
            );
            query.setParameter("email", user.getEmail());
            query.setParameter("carId", carId);
            query.setParameter("date", date);
            query.setParameter("beforeDate", beforeDate);
            query.setParameter("afterDate", afterDate);

            List<Set<Procedure>> proceduresList = query.list()
                    .stream().map(Visit::getProcedures).collect(Collectors.toList());

            List<Procedure> procedures = new ArrayList<>();

            for (Set<Procedure> list : proceduresList) {
                procedures.addAll(list);
            }

            return procedures;
        }
    }

    @Override
    public Procedure getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Procedure procedure = session.get(Procedure.class, id);
            if (procedure == null) {
                throw new EntityNotFoundException(PROCEDURE, id);
            }
            return procedure;
        }
    }

    @Override
    public Procedure getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Procedure> query = session.createQuery(
                    "from Procedure where name = :name", Procedure.class
            );
            query.setParameter("name", name);
            List<Procedure> procedures = query.list();
            Validator.ValidateListNotEmpty(procedures, PROCEDURE);
            return procedures.get(0);
        }
    }

    @Override
    public void create(Procedure procedure) {
        try (Session session = sessionFactory.openSession()) {
            session.save(procedure);
        }
    }

    @Override
    public void update(Procedure procedure) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(procedure);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Procedure procedureToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(procedureToDelete);
            session.getTransaction().commit();
        }
    }

}
