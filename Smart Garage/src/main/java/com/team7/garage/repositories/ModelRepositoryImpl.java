package com.team7.garage.repositories;

import com.team7.garage.contracts.ModelRepository;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.Manufacturer;
import com.team7.garage.models.Model;
import com.team7.garage.utils.Validator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl implements ModelRepository {

    private static final String MODEL = "Model";

    private static final String NAME = "name";

    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Model> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery(
                    "from Model", Model.class
            );
            return query.list();
        }
    }

    @Override
    public Model getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Model model = session.get(Model.class, id);
            if (model == null) {
                throw new EntityNotFoundException(MODEL, id);
            }
            return model;
        }
    }

    @Override
    public Model getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery(
                    "from Model where name = :name", Model.class
            );
            query.setParameter("name", name);
            List<Model> models = query.list();
            Validator.ValidateListNotEmpty(models, MODEL);
            return models.get(0);
        }
    }

    @Override
    public Model getByNameOrCreate(String name, Manufacturer manufacturer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery(
                    "from Model where name = :name", Model.class
            );
            query.setParameter("name", name);
            List<Model> models = query.list();
            if (models.size() == 0) {
                Model model = new Model(name, manufacturer);
                create(model);
                return model;
            }
            return models.get(0);
        }
    }

    @Override
    public void create(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.save(model);
        }
    }

    @Override
    public void update(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(model);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Model modelToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(modelToDelete);
            session.getTransaction().commit();
        }
    }

}
