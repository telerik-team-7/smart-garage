package com.team7.garage.repositories;

import com.team7.garage.contracts.ManufacturerRepository;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.Manufacturer;
import com.team7.garage.utils.Validator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManufacturerRepositoryImpl implements ManufacturerRepository {

    private static final String MANUFACTURER = "Manufacturer";

    private static final String NAME = "name";

    private final SessionFactory sessionFactory;

    @Autowired
    public ManufacturerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Manufacturer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Manufacturer> query = session.createQuery(
                    "from Manufacturer", Manufacturer.class
            );
            return query.list();
        }
    }

    @Override
    public Manufacturer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Manufacturer manufacturer = session.get(Manufacturer.class, id);
            if (manufacturer == null) {
                throw new EntityNotFoundException(MANUFACTURER, id);
            }
            return manufacturer;
        }
    }

    @Override
    public Manufacturer getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Manufacturer> query = session.createQuery(
                    "from Manufacturer where name = :name", Manufacturer.class
            );
            query.setParameter("name", name);
            List<Manufacturer> manufacturers = query.list();
            Validator.ValidateListNotEmpty(manufacturers, MANUFACTURER);
            return manufacturers.get(0);
        }
    }

    @Override
    public Manufacturer getByNameOrCreate(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Manufacturer> query = session.createQuery(
                    "from Manufacturer where name = :name", Manufacturer.class
            );
            query.setParameter("name", name);
            List<Manufacturer> manufacturers = query.list();
            if (manufacturers.size() == 0) {
                Manufacturer manufacturer = new Manufacturer(name);
                create(manufacturer);
                return manufacturer;
            }
            return manufacturers.get(0);
        }
    }

    @Override
    public void create(Manufacturer manufacturer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(manufacturer);
        }
    }

    @Override
    public void update(Manufacturer manufacturer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(manufacturer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Manufacturer manufacturerToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(manufacturerToDelete);
            session.getTransaction().commit();
        }
    }

}
