package com.team7.garage.controllers.mvc;

import com.team7.garage.contracts.UserService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;

    private final UserService userService;


    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper, UserService userService) {

        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isCustomer")
    public boolean populateIsCustomer(HttpSession session) {
        if (session.getAttribute("currentUser") == null) {
            return false;
        }
        User user = authenticationHelper.tryGetUser(session);
        return !user.isEmployee() && !user.isAdmin();
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("currentUser") == null) {
            return false;
        }
        User user = authenticationHelper.tryGetUser(session);
        return user.isEmployee() || user.isAdmin();
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") == null) {
            return false;
        }
        User user = authenticationHelper.tryGetUser(session);
        return user.isAdmin();
    }

    @ModelAttribute("customerId")
    public Integer populateCustomerId(HttpSession session) {
        if (session.getAttribute("currentUser") == null) {
            return 0;
        }
        User user = authenticationHelper.tryGetUser(session);
        return user.getId();
    }


    @GetMapping
    public String showHomePage(Model model) {
        return "index";
    }

    @GetMapping("/error")
    public String showErrorPage(Model model) {
        model.addAttribute("error", "Page Not Found");
        return "not-found";
    }
}
