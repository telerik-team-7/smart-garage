package com.team7.garage.controllers.mvc;

import com.team7.garage.contracts.UserService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.User;
import com.team7.garage.utils.UrlGenerator;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Controller
public class ForgottenPasswordController {

    @Autowired
    private UserService userService;

    @Autowired
    private JavaMailSender mailSender;

    @GetMapping("/forgotten_password")
    public String showForgotPasswordPage(Model model) {
        model.addAttribute("pageTitle", "Forgotten Password");
        return "forgotten_password_form";
    }

    @PostMapping("/forgotten_password")
    public String processForgotPasswordForm(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);

        try {
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = UrlGenerator.getSiteURL(request) + "/reset_password?token=" + token;

            sendEmail(email, resetPasswordLink);

            model.addAttribute("message", "We have sent a link to your e-mail. Please check your e-mail.");

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending e-mail.");
        }
        model.addAttribute("pageTitle", "Forgotten Password");
        return "forgotten_password_form";
    }

    private void sendEmail(String email, String resetPasswordLink) throws UnsupportedEncodingException, MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("contact@smart-garage.com", "Smart Garage Support");
        helper.setTo(email);
        String subject = "Here's the link to reset your password";
        String content = "<p>Hello,</p>"
                + "<p>You have requested to change your password:</p>"
                + "<p><b><a href=\"" + resetPasswordLink + "\">Change my password</a></p>";
        helper.setSubject(subject);
        helper.setText(content, true);

        mailSender.send(message);
    }

    @GetMapping("/reset_password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        try {
            User user = userService.getToken(token);
            if (user == null) {
                model.addAttribute("message", "Invalid token");
                model.addAttribute("title", "Token mismatch");
                return "message";
            }
        }catch (EntityNotFoundException e){
            model.addAttribute("message", "Invalid token");
            model.addAttribute("title", "Token mismatch");
            return "message";
        }
        model.addAttribute("token",token);

        return "reset_password_form";
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) {
       String token = request.getParameter("token");
       String password = request.getParameter("password");

       User user;
        try {
            user = userService.getToken(token);
        }catch (EntityNotFoundException e){
            model.addAttribute("message", "Invalid token");
            model.addAttribute("title", "Token mismatch");
            return "message";
        }
        if (user == null) {
            model.addAttribute("message", "Invalid token");
            model.addAttribute("title", "Reset your password");
            return "message";
        }else {
            userService.updatePassword(password,user);
            model.addAttribute("message", "You have successfully changed your password.");
        }
        return "message";
    }
}
