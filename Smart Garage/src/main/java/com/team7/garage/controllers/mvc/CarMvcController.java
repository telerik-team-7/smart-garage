package com.team7.garage.controllers.mvc;

import com.team7.garage.contracts.CarService;
import com.team7.garage.contracts.UserService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.*;
import com.team7.garage.models.*;
import com.team7.garage.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/cars")
public class CarMvcController {
    private static final String ACCESS_DENIED = "Only employees can access this page.";

    private final CarService carService;

    private final UserService userService;

    private final AuthenticationHelper authenticationHelper;

    private final ModelMapper modelMapper;

    @Autowired
    public CarMvcController(CarService carService,
                            UserService userService,
                            AuthenticationHelper authenticationHelper,
                            ModelMapper modelMapper) {
        this.carService = carService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll(authenticationHelper.getAdmin());
    }

    @GetMapping
    public String showAllCars(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("cars", carService.getAll(user));
        return "cars";
    }

    @GetMapping("/{id}")
    public String showSingleCar(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Car car = carService.getById(id, user);
            model.addAttribute("car", car);
            return "car";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/new")
    public String showNewCarPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("car", new CarDto());
        return "car-new";
    }

    @PostMapping("/new")
    public String createCar(@Valid @ModelAttribute("car") CarDto carDto,
                            BindingResult errors,
                            HttpSession session,
                            Model model) {
        if (errors.hasErrors()) {
            return "car-new";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Car car = modelMapper.fromDto(carDto);
            carService.create(car, user);
            return "redirect:/cars";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DuplicateEntityException | InvalidRegistrationNumberException e) {
            errors.rejectValue("regNumber", "error", e.getMessage());
            return "car-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditCarPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Car car = carService.getById(id, user);
            CarDto carDto = modelMapper.toDto(car);
            model.addAttribute("carId", id);
            model.addAttribute("car", carDto);
            return "car-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/{id}/update")
    public String updateCar(@PathVariable int id,
                            @Valid @ModelAttribute("car") CarDto carDto,
                            BindingResult errors,
                            HttpSession session,
                            Model model) {
        if (errors.hasErrors()) {
            return "car-update";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Car car = modelMapper.fromDto(carDto, id);
            carService.update(car, user);

            return "redirect:/cars";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (InvalidRegistrationNumberException e) {
            errors.rejectValue("regNumber", "error", e.getMessage());
            return "car-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCar(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            carService.delete(id, user);

            return "redirect:/cars";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (RequestDeniedException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }
}
