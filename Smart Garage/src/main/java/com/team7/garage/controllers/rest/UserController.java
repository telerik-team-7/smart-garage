package com.team7.garage.controllers.rest;

import com.team7.garage.contracts.UserService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.models.Car;
import com.team7.garage.models.User;
import com.team7.garage.models.UserDto;
import com.team7.garage.utils.ModelMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService,
                          ModelMapper modelMapper,
                          AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Returns a list of all users",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<User> getUsers(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAll(user);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Finds a user by id",
            notes = "Requires employee level authorization.",
            response = User.class)
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    @ApiOperation(value = "Returns a list of users filtered by given parameters",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<User> search(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) String email,
                             @RequestParam(required = false) String firstName,
                             @RequestParam(required = false) String lastName,
                             @RequestParam(required = false) String phone
    ) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.search(email, firstName, lastName, phone, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping
    @ApiOperation(value = "Registers a user from a DTO",
            notes = "Can by accessed without authorization",
            response = User.class)
    public User create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = modelMapper.fromDto(userDto);
            userService.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Updates an existing user from a DTO",
            notes = "Requires employee level authorization (Unless user is updating his own data).",
            response = User.class)
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody UserDto userDto) {
        try {
            User userAuth = authenticationHelper.tryGetUser(headers);
            User user = modelMapper.fromDto(userDto, id);
            userService.update(user, userAuth);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletes an existing user",
            notes = "Requires employee level authorization (Unless a user is deleting his own profile).")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    @ApiOperation(value = "Returns a list of users filtered by given parameters",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<User> filter(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) String email,
                             @RequestParam(required = false) String firstName,
                             @RequestParam(required = false) String lastName,
                             @RequestParam(required = false) String phone) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.search(email,
                    firstName,
                    lastName,
                    phone, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}