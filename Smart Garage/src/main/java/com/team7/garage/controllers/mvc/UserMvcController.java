package com.team7.garage.controllers.mvc;

import com.team7.garage.contracts.CarService;
import com.team7.garage.contracts.RoleService;
import com.team7.garage.contracts.UserService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.*;
import com.team7.garage.models.*;
import com.team7.garage.utils.ModelMapper;
import com.team7.garage.utils.PasswordEmailSender;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;



@Controller
@RequestMapping("/users")
public class UserMvcController {

    private static final String ACCESS_DENIED = "Only employees can access this page.";

    @Autowired
    private JavaMailSender mailSender;

    private final PasswordEmailSender emailSender;
    private final UserService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final RoleService roleService;
    private final CarService carService;


    @Autowired
    public UserMvcController(PasswordEmailSender emailSender, UserService service, AuthenticationHelper authenticationHelper, ModelMapper modelMapper, RoleService roleService, CarService carService) {
        this.emailSender = emailSender;
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.roleService = roleService;
        this.carService = carService;
    }

    @ModelAttribute("cars")
    public List<Car> populateCars() {
        return carService.getAll(authenticationHelper.getAdmin());
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("currentUser") == null) {
            return false;
        }
        User user = authenticationHelper.tryGetUser(session);
        return user.isEmployee();
    }

    @ModelAttribute("isUser")
    public boolean populateIsUser(HttpSession session) {
        if (session.getAttribute("currentUser") == null) {
            return false;
        }
        User user = authenticationHelper.tryGetUser(session);
        return user.isUser();
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") == null) {
            return false;
        }
        User user = authenticationHelper.tryGetUser(session);
        return user.isAdmin();
    }

    @ModelAttribute("roles")
    public List<Role> populateRoles() {
        return roleService.getAll();
    }


    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("users", service.getAll(user));
            return "users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
               User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            User newUser = service.getById(id,user);
            model.addAttribute("user", newUser);
            model.addAttribute("cars", carService.getUserCars(newUser));
            model.addAttribute("available", (user.getId() == id || user.isAdmin()));
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }


    @GetMapping("/profile")
    public String showProfile(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("user", user);
            model.addAttribute("cars", carService.getUserCars(user));

            return "user";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewUserPage(Model model) {

        model.addAttribute("user", new UserDto());
        return "user-new";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") UserDto userDto,
                                 BindingResult errors,
                                 Model model,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return "user-new";
        }

        User userAuth;
        try {
            userAuth = authenticationHelper.tryGetUser(session);
            User userNew = modelMapper.fromDto(userDto);
            service.create(userNew);
            return "redirect:/";

        } catch (DuplicateEntityException e) {
                errors.rejectValue("email", "duplicate_user", e.getMessage());
                return "user-new";
            } catch (EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "not-found";
            }

    }

    @GetMapping("/login")
    public String showLoginPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("user", new LoginDto());
            return "login";
        }
        return "redirect:/";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("user") LoginDto loginDto,
                              BindingResult errors,
                              HttpSession session) {
        if (errors.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(loginDto.getEmail(), loginDto.getPassword());
            session.setAttribute("currentUser", loginDto.getEmail());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            errors.rejectValue("email", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (user.getId() != id && !user.isAdmin()) {
            model.addAttribute("error", ACCESS_DENIED);
            return "forbidden";
        }

        try {
            User newUser = service.getById(id,user);
            UserDto newUserDto = modelMapper.toDto(newUser);
            model.addAttribute("newUserId", id);
            model.addAttribute("user", newUserDto);
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                                 @Valid @ModelAttribute("newUser") UserDto newUserDto,
                                 HttpSession session,
                                 BindingResult errors,
                                 Model model) {

        if (errors.hasErrors()) {
            return "user-update";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            User newUser = modelMapper.fromDto(newUserDto, id);
            service.update(newUser,user);

            return "redirect:/users/profile";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_newUser", e.getMessage());
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (user.getId() != id && !user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            User newUser = service.getById(id,user);
            if (user.getId() == id) {
                session.removeAttribute("currentUser");
            }

            service.delete(id, user);

            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        } catch (RequestDeniedException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }
}
