package com.team7.garage.controllers;

import com.team7.garage.contracts.UserService;
import com.team7.garage.exceptions.AuthenticationFailureException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.models.Role;
import com.team7.garage.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.util.Set;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong email or password.";
    private static final String UNAUTHORIZED_STATUS = "The requested resource requires authentication.";
    private static final String BAD_REQUEST_STATUS = "E-mail does not exist.";
    private static final String CURRENT_USER = "currentUser";
    private static final String NO_USER_LOGGED_IN = "No user logged in.";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_STATUS);
        }

        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST_STATUS);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute(CURRENT_USER);

        if (currentUser == null) {
            throw new AuthenticationFailureException(NO_USER_LOGGED_IN);
        }

        try {
            return userService.getByEmail(currentUser);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(NO_USER_LOGGED_IN);
        }
    }

    public User verifyAuthentication(String email, String password) {
        try {
            User user = userService.getByEmail(email);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

    public User getAdmin() {
        User employee = new User();
        employee.setFirstName("Garrus");
        employee.setLastName("Vakarian");
        employee.setEmail("iShootBottles@Citadel.com");
        Role admin = new Role();
        admin.setName("Admin");
        Role employeeRole = new Role();
        employeeRole.setName("Employee");
        employee.setRoles(Set.of(employeeRole, admin));

        return employee;
    }
}
