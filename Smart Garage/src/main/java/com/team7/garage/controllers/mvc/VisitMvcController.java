package com.team7.garage.controllers.mvc;

import com.team7.garage.contracts.CarService;
import com.team7.garage.contracts.ProcedureService;
import com.team7.garage.contracts.VisitService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.AuthenticationFailureException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.InvalidDateException;
import com.team7.garage.exceptions.InvalidRegistrationNumberException;
import com.team7.garage.models.*;
import com.team7.garage.utils.ModelMapper;
import com.team7.garage.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    private static final String ACCESS_DENIED = "Only employees can access this page.";


    private final VisitService visitService;

    private final ProcedureService procedureService;

    private final CarService carService;

    private final AuthenticationHelper authenticationHelper;

    private final ModelMapper modelMapper;

    @Autowired
    public VisitMvcController(VisitService visitService,
                              ProcedureService procedureService,
                              CarService carService,
                              AuthenticationHelper authenticationHelper,
                              ModelMapper modelMapper) {
        this.visitService = visitService;
        this.procedureService = procedureService;
        this.carService = carService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("cars")
    public List<Car> populateCars() {
        return carService.getAll(authenticationHelper.getAdmin());
    }

    @ModelAttribute("procedures")
    public List<Procedure> populateProcedures() {
        return procedureService.getAll(authenticationHelper.getAdmin());
    }

    @GetMapping
    public String showAllVisits(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("visits", visitService.getAll(user, "BGN"));
        return "visits";
    }

    @GetMapping("/{id}")
    public String showSingleVisit(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            VisitResponseDto visit = visitService.getById(id, "BGN", user);
            model.addAttribute("visit", visit);
            return "visit";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/new")
    public String showNewVisitPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("visit", new VisitDto());
        return "visit-new";
    }

    @PostMapping("/new")
    public String createVisit(@Valid @ModelAttribute("visit") VisitDto visitDto,
                              BindingResult errors,
                              HttpSession session,
                              Model model) {
        if (errors.hasErrors()) {
            return "visit-new";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Visit visit = modelMapper.fromDto(visitDto);
            visitService.create(visit, user);
            return "redirect:/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (InvalidDateException e) {
            errors.rejectValue("endDate", "code", e.getMessage());
            return "visit-update";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/new-for-new-user")
    public String showNewVisitAndUserPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("visit", new NewUserVisitDto());
        return "visit-new-for-new-user";
    }

    @PostMapping("/new-for-new-user")
    public String createVisit(@Valid @ModelAttribute("visit") NewUserVisitDto visitDto,
                              BindingResult errors,
                              HttpSession session,
                              Model model) {
        if (errors.hasErrors()) {
            return "visit-new-for-new-user";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Visit visit = modelMapper.fromDto(visitDto);
            visitService.create(visit, user);
            return "redirect:/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (InvalidRegistrationNumberException e) {
            errors.rejectValue("regNumber", "error", e.getMessage());
            return "car-new";
        } catch (InvalidDateException e) {
            errors.rejectValue("endDate", "code", e.getMessage());
            return "visit-new-for-new-user";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditVisitPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            VisitResponseDto visit = visitService.getById(id, "BGN", user);
            VisitDto visitDto = modelMapper.toDto(visit);
            model.addAttribute("visitId", id);
            model.addAttribute("visit", visitDto);
            return "visit-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/{id}/update")
    public String updateVisit(@PathVariable int id,
                              @Valid @ModelAttribute("visit") VisitDto visitDto,
                              BindingResult errors,
                              HttpSession session,
                              Model model) {
        if (errors.hasErrors()) {
            return "visit-update";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Visit visit = modelMapper.fromDto(visitDto, id);
            visitService.update(visit, user);

            return "redirect:/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (InvalidDateException e) {
            errors.rejectValue("endDate", "code", e.getMessage());
            return "visit-update";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }


    @GetMapping("/{id}/delete")
    public String deleteVisit(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            visitService.delete(id, user);

            return "redirect:/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }
}
