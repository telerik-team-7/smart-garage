package com.team7.garage.controllers.rest;

import com.team7.garage.contracts.VisitService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.*;
import com.team7.garage.models.*;
import com.team7.garage.utils.ModelMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private static final String CURRENCY = "BGN";
    private final VisitService visitService;

    private final ModelMapper modelMapper;

    private final AuthenticationHelper authenticationHelper;

    public VisitController(VisitService visitService,
                           ModelMapper modelMapper,
                           AuthenticationHelper authenticationHelper) {
        this.visitService = visitService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Returns a list of all visits to the service",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<VisitResponseDto> getAll(@RequestParam(required = false) String currency,
                                         @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return visitService.getAll(user, currency);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns a visit by id",
            notes = "Requires employee level authorization.",
            response = Visit.class)
    public VisitResponseDto getById(@PathVariable int id,
                                    @RequestParam(required = false) String currency,
                                    @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return visitService.getById(id, currency, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InvalidCurrencyException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Creates a visit from a DTO",
            notes = "Requires employee level authorization.",
            response = Visit.class)
    public VisitResponseDto create(@Valid @RequestBody VisitDto visitDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit visit = modelMapper.fromDto(visitDto);
            visitService.create(visit, user);
            return modelMapper.toResponseDto(visit, CURRENCY);
        } catch (InvalidDateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/new-user")
    @ApiOperation(value = "Creates a visit, user and car from a DTO",
            notes = "Requires employee level authorization.",
            response = Visit.class)
    public VisitResponseDto create(@Valid @RequestBody NewUserVisitDto visitDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit visit = modelMapper.fromDto(visitDto);
            visitService.create(visit, user);
            return modelMapper.toResponseDto(visit, CURRENCY);
        } catch (InvalidDateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Updates an existing visit from a DTO",
            notes = "Requires employee level authorization.",
            response = Visit.class)
    public VisitResponseDto update(@PathVariable int id, @Valid @RequestBody VisitDto visitDto,
                                   @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit visit = modelMapper.fromDto(visitDto, id);
            visitService.update(visit, user);
            return modelMapper.toResponseDto(visit, CURRENCY);
        } catch (InvalidDateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletes an existing visit by id",
            notes = "Requires employee level authorization.")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            visitService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
