package com.team7.garage.controllers.mvc;

import com.team7.garage.contracts.ProcedureService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.AuthenticationFailureException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.RequestDeniedException;
import com.team7.garage.models.Procedure;
import com.team7.garage.models.ProcedureDto;
import com.team7.garage.models.User;
import com.team7.garage.utils.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/procedures")
public class ProcedureMvcController {

    private static final String ACCESS_DENIED = "Only employees can access this page.";
    private final ProcedureService procedureService;

    private final AuthenticationHelper authenticationHelper;

    private final ModelMapper modelMapper;

    public ProcedureMvcController(ProcedureService procedureService,
                                  AuthenticationHelper authenticationHelper,
                                  ModelMapper modelMapper) {
        this.procedureService = procedureService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public String showAllProcedures(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("procedures", procedureService.getAll(user));
        return "procedures";
    }

    @GetMapping("/{id}")
    public String showSingleProcedure(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Procedure procedure = procedureService.getById(id, user);
            model.addAttribute("procedure", procedure);
            return "procedure";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/new")
    public String showNewParcelPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("procedure", new ProcedureDto());
        return "procedure-new";
    }

    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("procedure") ProcedureDto procedureDto,
                               BindingResult errors,
                               HttpSession session,
                               Model model) {
        if (errors.hasErrors()) {
            return "procedure-new";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Procedure procedure = modelMapper.fromDto(procedureDto);
            procedureService.create(procedure, user);
            return "redirect:/procedures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditProcedurePage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Procedure procedure = procedureService.getById(id, user);
            ProcedureDto procedureDto = modelMapper.toDto(procedure);
            model.addAttribute("procedureId", id);
            model.addAttribute("procedure", procedureDto);
            return "procedure-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/{id}/update")
    public String updateProcedure(@PathVariable int id,
                               @Valid @ModelAttribute("procedure") ProcedureDto procedureDto,
                               BindingResult errors,
                               HttpSession session,
                               Model model) {
        if (errors.hasErrors()) {
            return "procedure-update";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            Procedure procedure = modelMapper.fromDto(procedureDto, id);
            procedureService.update(procedure, user);

            return "redirect:/procedures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteProcedure(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!user.isAdmin()) {
                model.addAttribute("error", ACCESS_DENIED);
                return "forbidden";
            }
            procedureService.delete(id, user);

            return "redirect:/procedures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (RequestDeniedException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

}
