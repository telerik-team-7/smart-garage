package com.team7.garage.controllers.rest;

import com.team7.garage.contracts.ProcedureService;
import com.team7.garage.contracts.UserService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.models.Procedure;
import com.team7.garage.models.ProcedureDto;
import com.team7.garage.models.User;
import com.team7.garage.utils.ModelMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/procedures")
public class ProcedureController {

    private final ProcedureService procedureService;

    private final UserService userService;

    private final ModelMapper modelMapper;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ProcedureController(ProcedureService procedureService,
                               UserService userService,
                               ModelMapper modelMapper,
                               AuthenticationHelper authenticationHelper) {
        this.procedureService = procedureService;
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Returns a list of all procedures",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<Procedure> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return procedureService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/user")
    @ApiOperation(value = "Returns a list of all procedures linked to a user",
            notes = "Requires employee level authorization unless a user requests his own procedures.",
            response = List.class)
    public List<Procedure> getForUser(@RequestParam String email,
                                      @RequestParam(required = false)Integer carId,
                                      @RequestParam(required = false)String date,
                                      @RequestParam(required = false)String beforeDate,
                                      @RequestParam(required = false)String afterDate,
                                      @RequestHeader HttpHeaders headers) {
        try {
            User caller = authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(email);
            return procedureService.getForUser(user,
                    carId,
                    date,
                    beforeDate,
                    afterDate,
                    caller);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    @ApiOperation(value = "Returns a list of procedures filtered by given parameters",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<Procedure> filter(@RequestHeader HttpHeaders headers,
                            @RequestParam(required = false) String name,
                            @RequestParam(required = false) Double price,
                            @RequestParam(required = false) Double minPrice,
                            @RequestParam(required = false) Double maxPrice) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return procedureService.filter(name,
                    price,
                    minPrice,
                    maxPrice,
                    user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns a procedure by id",
            notes = "Requires employee level authorization.",
            response = Procedure.class)
    public Procedure getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return procedureService.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Creates a procedure from a DTO",
            notes = "Requires employee level authorization.",
            response = Procedure.class)
    public Procedure create(@Valid @RequestBody ProcedureDto procedureDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Procedure procedure = modelMapper.fromDto(procedureDto);
            procedureService.create(procedure, user);
            return procedure;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Updates an existing procedure from a DTO",
            notes = "Requires employee level authorization.",
            response = Procedure.class)
    public Procedure update(@PathVariable int id, @Valid @RequestBody ProcedureDto procedureDto,
                            @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Procedure procedure = modelMapper.fromDto(procedureDto, id);
            procedureService.update(procedure, user);
            return procedure;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletes an existing procedure by id",
            notes = "Requires employee level authorization.")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            procedureService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
