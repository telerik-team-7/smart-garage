package com.team7.garage.controllers.rest;

import com.team7.garage.contracts.CarService;
import com.team7.garage.controllers.AuthenticationHelper;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.InvalidRegistrationNumberException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.models.Car;
import com.team7.garage.models.CarDto;
import com.team7.garage.models.User;
import com.team7.garage.utils.ModelMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/cars")
public class CarController {

    private final CarService carService;

    private final ModelMapper modelMapper;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CarController(CarService carService,
                         ModelMapper modelMapper,
                         AuthenticationHelper authenticationHelper) {
        this.carService = carService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @ApiOperation(value = "Returns a list of all cars",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<Car> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return carService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    @ApiOperation(value = "Returns a list of cars filtered by given parameters",
            notes = "Requires employee level authorization.",
            response = List.class)
    public List<Car> filter(@RequestHeader HttpHeaders headers,
                            @RequestParam(required = false) String vin,
                            @RequestParam(required = false) String regNumber,
                            @RequestParam(required = false) String model,
                            @RequestParam(required = false) String manufacturer,
                            @RequestParam(required = false) String userFirstName,
                            @RequestParam(required = false) String userLastName,
                            @RequestParam(required = false) String userEmail) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return carService.filter(vin,
                    regNumber,
                    model,
                    manufacturer,
                    userFirstName,
                    userLastName,
                    userEmail,
                    user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns a car by id",
            notes = "Requires employee level authorization.",
            response = Car.class)
    public Car getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return carService.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "Creates a car from a DTO",
            notes = "Requires employee level authorization.",
            response = Car.class)
    public Car create(@Valid @RequestBody CarDto carDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Car car = modelMapper.fromDto(carDto);
            carService.create(car, user);
            return car;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InvalidRegistrationNumberException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Updates and existing car from a DTO",
            notes = "Requires employee level authorization.",
            response = Car.class)
    public Car update(@PathVariable int id, @Valid @RequestBody CarDto carDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Car car = modelMapper.fromDto(carDto, id);
            carService.update(car, user);
            return car;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletes and existing car by id",
            notes = "Requires employee level authorization.")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            carService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}