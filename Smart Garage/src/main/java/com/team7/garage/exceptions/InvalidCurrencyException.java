package com.team7.garage.exceptions;

public class InvalidCurrencyException extends RuntimeException{

    public InvalidCurrencyException(String message) {
        super(message);
    }

}
