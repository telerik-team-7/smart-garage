package com.team7.garage.exceptions;

public class RequestDeniedException extends RuntimeException{
    public RequestDeniedException(String message) {
        super(message);
    }
}
