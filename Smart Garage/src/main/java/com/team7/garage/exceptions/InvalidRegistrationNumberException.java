package com.team7.garage.exceptions;

public class InvalidRegistrationNumberException extends RuntimeException {
    public InvalidRegistrationNumberException(String message) {
        super(message);
    }
}
