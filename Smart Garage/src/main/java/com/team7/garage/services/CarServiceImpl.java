package com.team7.garage.services;

import com.team7.garage.contracts.*;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.RequestDeniedException;
import com.team7.garage.models.Car;
import com.team7.garage.models.Model;
import com.team7.garage.models.User;
import com.team7.garage.models.Visit;
import com.team7.garage.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private static final String REGISTRATION = "Car with registration number or VIN";
    private static final String CAR = "Car";
    private static final String CANNOT_DELETE_CARS_WITH_REGISTERED_VISITS = "Cannot delete cars with registered visits";
    private final CarRepository  carRepository;

    private final VisitRepository visitRepository;

    private final ManufacturerRepository manufacturerRepository;

    private final ModelRepository modelRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository,
                          VisitRepository visitRepository,
                          ManufacturerRepository manufacturerRepository,
                          ModelRepository modelRepository) {
        this.carRepository = carRepository;
        this.visitRepository = visitRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Car> getAll(User user) {
        Validator.ValidateAuthorization(user);
        return carRepository.getAll();
    }

    @Override
    public Car getById(int id, User user) {
        Validator.ValidateAuthorization(user);
        return carRepository.getById(id);
    }

    @Override
    public List<Car> filter(String vin,
                            String regNumber,
                            String model,
                            String manufacturer,
                            String userFirstName,
                            String userLastName,
                            String userEmail,
                            User user) {

        Validator.ValidateAuthorization(user);

        return carRepository.filter(vin,
                regNumber,
                model,
                manufacturer,
                userFirstName,
                userLastName,
                userEmail);
    }

    @Override
    public void create(Car car, User user) {
        Validator.ValidateAuthorization(user);

        boolean duplicateExists = true;
        try {
            carRepository.getByRegistration(car.getRegNumber());

        } catch (EntityNotFoundException e) {
            try {
                carRepository.getByVIN(car.getVin());
            } catch (EntityNotFoundException e1) {
                duplicateExists = false;
            }
        }

        if (duplicateExists) {
            rollbackCheck(car.getModel());
            throw new DuplicateEntityException(REGISTRATION);
        }

        carRepository.create(car);
    }

    @Override
    public void update(Car car, User user) {
        Validator.ValidateAuthorization(user);

        boolean duplicateExists = true;
        try {
            Car existingCar = carRepository.getByRegistration(car.getRegNumber());
            if (existingCar.getId() == car.getId() && existingCar.getVin().equals(car.getVin())) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(CAR, REGISTRATION, car.getRegNumber());
        }

        carRepository.update(car);
    }

    @Override
    public void delete(int id, User user) {
        Validator.ValidateAuthorization(user);

        List<Visit> visits = visitRepository.getByCarId(id);
        if (visits.size() != 0) {
            throw new RequestDeniedException(CANNOT_DELETE_CARS_WITH_REGISTERED_VISITS);
        }

        carRepository.delete(id);
    }

    @Override
    public List<Car> getUserCars(User user) {
        return carRepository.getUserCars(user);
    }

    private Car getByRegistration(String registration) {
        return carRepository.getByRegistration(registration);
    }

    private void rollbackCheck(Model model) {
        List<Car> carsByModel = carRepository.getByModel(model.getName());

        if (carsByModel.size() == 0) {
            modelRepository.delete(model.getId());
        }

        List<Car> carsByManufacturer = carRepository.getByManufacturer(model.getManufacturer().getName());

        if (carsByManufacturer.size() == 0) {
            manufacturerRepository.delete(model.getManufacturer().getId());
        }


    }
}
