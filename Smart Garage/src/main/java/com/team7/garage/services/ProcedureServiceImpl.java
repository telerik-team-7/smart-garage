package com.team7.garage.services;

import com.team7.garage.contracts.ProcedureRepository;
import com.team7.garage.contracts.ProcedureService;
import com.team7.garage.contracts.VisitRepository;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.RequestDeniedException;
import com.team7.garage.models.Procedure;
import com.team7.garage.models.User;
import com.team7.garage.models.Visit;
import com.team7.garage.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ProcedureServiceImpl implements ProcedureService {
    private static final String NAME = "name";

    private static final String SERVICE = "Service";
    private static final String CANNOT_DELETE_PROCEDURES_REGISTERED_IN_VISITS = "Cannot delete procedures registered in visits";
    private final ProcedureRepository procedureRepository;

    private final VisitRepository visitRepository;

    @Autowired
    public ProcedureServiceImpl(ProcedureRepository procedureRepository,
                                VisitRepository visitRepository) {
        this.procedureRepository = procedureRepository;
        this.visitRepository = visitRepository;
    }

    @Override
    public List<Procedure> getAll(User user) {
        Validator.ValidateAuthorization(user);
        return procedureRepository.getAll();
    }

    @Override
    public List<Procedure> getForUser(User user,
                                      Integer carId,
                                      String date,
                                      String beforeDate,
                                      String afterDate,
                                      User caller) {
        Validator.ValidateAccess(user, caller);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate;
        LocalDate localBeforeDate;
        LocalDate localAfterDate;
        if (date != null) {
            localDate = LocalDate.parse(date, formatter);
        } else localDate = null;

        if (beforeDate != null) {
            localBeforeDate = LocalDate.parse(beforeDate, formatter);
        } else localBeforeDate = null;

        if (afterDate != null) {
            localAfterDate = LocalDate.parse(afterDate, formatter);
        } else localAfterDate = null;

        return procedureRepository.getForUser(user,
                carId,
                localDate,
                localBeforeDate,
                localAfterDate);
    }

    @Override
    public List<Procedure> filter(String name,
                                  Double price,
                                  Double minPrice,
                                  Double maxPrice,
                                  User user) {
        Validator.ValidateAuthorization(user);
        return procedureRepository.filter(name,
                price,
                minPrice,
                maxPrice);
    }

    @Override
    public Procedure getById(int id, User user) {
        Validator.ValidateAuthorization(user);
        return procedureRepository.getById(id);
    }

    @Override
    public Procedure getByName(String name, User user) {
        Validator.ValidateAuthorization(user);
        return procedureRepository.getByName(name);
    }

    @Override
    public void create(Procedure procedure, User user) {
        Validator.ValidateAuthorization(user);

        boolean duplicateExists = true;
        try {
            procedureRepository.getByName(procedure.getName());

        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(SERVICE, NAME, procedure.getName());
        }

        procedureRepository.create(procedure);
    }

    @Override
    public void update(Procedure procedure, User user) {
        Validator.ValidateAuthorization(user);

        boolean duplicateExists = true;
        try {
            Procedure existingProcedure = procedureRepository.getByName(procedure.getName());
            if (existingProcedure.getId() == procedure.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(SERVICE, NAME, procedure.getName());
        }

        procedureRepository.update(procedure);
    }

    @Override
    public void delete(int id, User user) {
        Validator.ValidateAuthorization(user);

        List<Visit> visits = visitRepository.getByProcedureId(id);
        if (visits.size() != 0) {
            throw new RequestDeniedException(CANNOT_DELETE_PROCEDURES_REGISTERED_IN_VISITS);
        }

        procedureRepository.delete(id);
    }
}