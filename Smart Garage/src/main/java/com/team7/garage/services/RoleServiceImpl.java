package com.team7.garage.services;

import com.team7.garage.contracts.RoleRepository;
import com.team7.garage.contracts.RoleService;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.models.Role;
import com.team7.garage.models.User;
import com.team7.garage.utils.Validator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository repository;

    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Role> getAll() {

        return repository.getAll();
    }

    @Override
    public Role getEmployeeRole() {
        return repository.getById(2);
    }

    @Override
    public void create(Role role, User user) {
        Validator.ValidateAuthorization(user);

        boolean duplicateExists = true;
        try {
            repository.getByName(role.getName());

        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("role");
        }
        repository.create(role);
    }

    @Override
    public void update(Role role, User user) {
        Validator.ValidateAuthorization(user);
        boolean duplicateExists = true;
        try {
            Role existingRole = repository.getByName(role.getName());
            if (existingRole.getId() == role.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("user", "role", role.getName());
        }
        repository.update(role);
    }

    @Override
    public void delete(int id, User user) {
        Validator.ValidateAuthorization(user);
        repository.delete(id);
    }

}
