package com.team7.garage.services;

import com.team7.garage.contracts.VisitRepository;
import com.team7.garage.contracts.VisitService;
import com.team7.garage.models.User;
import com.team7.garage.models.Visit;
import com.team7.garage.models.VisitResponseDto;
import com.team7.garage.utils.EmailSender;
import com.team7.garage.utils.ModelMapper;
import com.team7.garage.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VisitServiceImpl implements VisitService {
    private static final String EMAIL_SUBJECT = "Registered visit to Smart Garage";
    private final VisitRepository visitRepository;
    private final ModelMapper modelMapper;

    private final EmailSender emailSender;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository,
                            ModelMapper modelMapper,
                            EmailSender emailSender) {
        this.visitRepository = visitRepository;
        this.modelMapper = modelMapper;
        this.emailSender = emailSender;
    }

    @Override
    public List<VisitResponseDto> getAll(User user, String currency) {
        Validator.ValidateAuthorization(user);
        List<Visit> visits = visitRepository.getAll();
        List<VisitResponseDto> responseDtoList = new ArrayList<>();
        for (Visit visit :
                visits) {
            responseDtoList.add(modelMapper.toResponseDto(visit, currency));
        }
        return responseDtoList;
    }

    @Override
    public VisitResponseDto getById(int id, String currency, User user) {
        Validator.ValidateAuthorization(user);

        Visit visit = visitRepository.getById(id);

        return modelMapper.toResponseDto(visit, currency);
    }

    @Override
    public void create(Visit visit, User user) {
        Validator.ValidateAuthorization(user);
        Validator.ValidateDates(visit);

        emailSender.sendSimpleEmail(visit.getCar().getUser().getEmail(),
                emailSender.extractBody(visit),
                EMAIL_SUBJECT);

        visitRepository.create(visit);
    }


    @Override
    public void update(Visit visit, User user) {
        Validator.ValidateAuthorization(user);
        Validator.ValidateDates(visit);

        visitRepository.update(visit);
    }

    @Override
    public void delete(int id, User user) {
        Validator.ValidateAuthorization(user);
        visitRepository.delete(id);
    }
}
