package com.team7.garage.services;

import com.team7.garage.contracts.CarRepository;
import com.team7.garage.contracts.UserRepository;
import com.team7.garage.contracts.UserService;
import com.team7.garage.contracts.VisitRepository;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.RequestDeniedException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.models.Car;
import com.team7.garage.models.User;
import com.team7.garage.models.Visit;
import com.team7.garage.utils.EmailSender;
import com.team7.garage.utils.Validator;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private static final String INVALID_ROLE_ERROR = "Only employees can access this method";
    private static final String CANNOT_DELETE_USERS_WITH_REGISTERED_VISITS =
            "Cannot delete users with registered visits or cars";
    private final UserRepository repository;

    private final VisitRepository visitRepository;

    private final CarRepository carRepository;

    private final EmailSender emailSender;

    @Autowired
    public UserServiceImpl(UserRepository repository,
                           VisitRepository visitRepository,
                           CarRepository carRepository,
                           EmailSender emailSender) {
        this.repository = repository;
        this.visitRepository = visitRepository;
        this.carRepository = carRepository;
        this.emailSender = emailSender;
    }

    @Override
    public List<User> getAll(User userAuth) {
        if (!userAuth.isAdmin()) {
            throw new UnauthorizedOperationException(INVALID_ROLE_ERROR);
        }
        return repository.getAll();
    }


    @Override
    public User getById(int id, User userAuth) {
        return repository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public List<User> search(String email,
                             String firstName,
                             String lastName,
                             String phone,
                             User user) {

        Validator.ValidateAuthorization(user);
        return repository.search(email, firstName, lastName, phone);
    }


    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            repository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        String pass = RandomString.make(10);
        user.setPassword(pass);

        emailSender.SendPassword(user);

        repository.create(user);
    }

    @Override
    public void update(User user, User authUser) {
        if (user.getId() != authUser.getId() && !authUser.isAdmin()) {
            throw new UnauthorizedOperationException(INVALID_ROLE_ERROR);
        }
        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        repository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if (user.getId() != id && !user.isAdmin()) {
            throw new UnauthorizedOperationException(INVALID_ROLE_ERROR);
        }

        List<Visit> visits = visitRepository.getByUserId(id);
        List<Car> cars = carRepository.getUserCars(repository.getById(id));
        if (!cars.isEmpty() || !visits.isEmpty()) {
            throw new RequestDeniedException(CANNOT_DELETE_USERS_WITH_REGISTERED_VISITS);
        }
        repository.delete(id);
    }


    @Override
    public void updateResetPasswordToken(String token, String email) {
        User user = repository.getByEmail(email);
        if (user != null) {
            user.setToken(token);
            repository.update(user);
        } else {
            throw new EntityNotFoundException("user", "e-mail", email);
        }
    }

    @Override
    public User getByToken(String token, String email, User userAuth) {
        if (!userAuth.isAdmin()) {
            throw new UnauthorizedOperationException(INVALID_ROLE_ERROR);
        }
        return repository.getByToken(token);
    }

    @Override
    public void updatePassword(String newPass, User user) {
        user.setPassword(newPass);
        user.setToken(null);

        repository.update(user);
    }


    @Override
    public User getToken(String resetPasswordToken) {

        return repository.getByToken(resetPasswordToken);
    }


}
