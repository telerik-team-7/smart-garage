package com.team7.garage.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.Date;
import java.util.Set;

public class VisitDto {

    @Positive
    private int carId;

    @NotEmpty
    private Set<String> procedures;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private String endDate;

    public VisitDto() {
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public Set<String> getProcedures() {
        return procedures;
    }

    public void setProcedures(Set<String> procedures) {
        this.procedures = procedures;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
