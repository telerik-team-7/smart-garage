package com.team7.garage.models;

import javax.validation.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.Set;


public class UserDto {

    @NotEmpty(message = "First name cannot be null")
    @Size(min = 2, max = 15, message = "First name must be between 2 and 15 symbols")
    private String firstName;

    @NotEmpty(message = "Last name cannot be null")
    @Size(min = 2, max = 15, message = "Last name must be between 2 and 15 symbols")
    private String lastName;

    @NotEmpty(message = "Email cannot be null")
    private String email;

    /*@NotEmpty(message = "Password cannot be null")
    @Size(min = 2, max = 25)
    private String password;*/

    @NotEmpty(message = "Phone cannot be null")
    @Size(min = 10, max = 10, message = "Phone must have 10 symbols")
    private String phone;


    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

  /*  public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }*/

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
