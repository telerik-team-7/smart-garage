package com.team7.garage.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CarDto {

    @NotEmpty
    @Size(min = 17, max = 17, message = "VIN must be exactly 17 symbols long")
    private String vin;

    @NotEmpty
    @Size(min = 7, max = 8, message = "Registration number must be between 7 and 8 symbols")
    private String regNumber;

    @NotEmpty
    @Size(min = 2, max = 30, message = "Model must be between 4 and 30 symbols long")
    private String model;

    @NotEmpty
    @Size(min = 2, max = 30, message = "Manufacturer must be between 4 and 30 symbols long")
    private String manufacturer;

    @Positive
    private int userId;

    public CarDto() {
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
