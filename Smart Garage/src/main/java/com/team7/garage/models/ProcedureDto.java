package com.team7.garage.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ProcedureDto {

    @NotEmpty
    @Size(min = 4, max = 30, message = "Name must be between 4 and 30 symbols long")
    private String name;

    @Positive
    private Double price;

    public ProcedureDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
