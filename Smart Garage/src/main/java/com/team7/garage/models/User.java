package com.team7.garage.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.util.Set;
import java.util.stream.Collectors;


@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @JsonIgnore
    @Column(name = "password_reset_token")
    private String token;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;
    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "cars_users",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "cars_id")
    )
    private Set<Car> cars;


    public User() {
        this.token=null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    boolean isPhoneValid(){
        if (phone.length()!=10)
            return false;
        else return true;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public String printRoles(){
        StringBuilder builder = new StringBuilder();
         roles.stream().forEach(x->builder.append(String.format("%s, ",x.getName() )));
         if (builder.length()<2)
             return "";
         return builder.delete(builder.length()-2,builder.length()).toString();
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    @JsonIgnore
    public boolean isEmployee() {
        return getRoles().stream().anyMatch(role -> role.getName().equals("Employee"));
    }
    @JsonIgnore
    public boolean isAdmin() {
        return getRoles().stream().anyMatch(role -> role.getName().equals("Admin"));
    }
    @JsonIgnore
    public boolean isUser() {
        return getRoles().stream().anyMatch(role -> role.getName().equals("User"));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
