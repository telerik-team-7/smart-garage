package com.team7.garage.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

public class NewUserVisitDto {

    @NotNull
    private UserDto userDto;

    @NotEmpty
    @Size(min = 17, max = 17, message = "VIN must be exactly 17 symbols long")
    private String vin;

    @NotEmpty
    @Size(min = 7, max = 8, message = "Registration number must be between 7 and 8 symbols")
    private String regNumber;

    @NotEmpty
    @Size(min = 2, max = 30, message = "Model must be between 4 and 30 symbols long")
    private String model;

    @NotEmpty
    @Size(min = 2, max = 30, message = "Manufacturer must be between 4 and 30 symbols long")
    private String manufacturer;

    @NotEmpty
    private Set<String> procedures;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date endDate;

    public NewUserVisitDto(){

    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Set<String> getProcedures() {
        return procedures;
    }

    public void setProcedures(Set<String> procedures) {
        this.procedures = procedures;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
