package com.team7.garage.contracts;

import com.team7.garage.models.Procedure;
import com.team7.garage.models.User;

import java.time.LocalDate;
import java.util.List;

public interface ProcedureRepository {
    List<Procedure> getAll();

    Procedure getById(int id);

    Procedure getByName(String name);

    void create(Procedure procedure);

    void update(Procedure procedure);

    void delete(int id);

    List<Procedure> filter(String name, Double price, Double minPrice, Double maxPrice);

    List<Procedure> getForUser(User user, Integer carId, LocalDate date, LocalDate beforeDate, LocalDate afterDate);
}
