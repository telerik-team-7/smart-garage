package com.team7.garage.contracts;

import com.team7.garage.models.User;
import com.team7.garage.models.Visit;
import com.team7.garage.models.VisitResponseDto;

import java.util.List;

public interface VisitService {
    List<VisitResponseDto> getAll(User user, String currency);

    VisitResponseDto getById(int id, String currency, User user);

    void create(Visit visit, User user);

    void update(Visit visit, User user);

    void delete(int id, User user);
}
