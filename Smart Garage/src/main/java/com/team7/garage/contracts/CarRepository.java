package com.team7.garage.contracts;

import com.team7.garage.models.Car;
import com.team7.garage.models.User;

import java.util.List;

public interface CarRepository {
    List<Car> getAll();

    Car getById(int id);

    List<Car> filter(String vin,
                     String regNumber,
                     String model,
                     String manufacturer,
                     String userFirstName,
                     String userLastName,
                     String userEmail);

    Car getByRegistration(String registration);

    List<Car> getByModel(String model);

    List<Car> getByManufacturer(String manufacturer);

    Car getByVIN(String vin);

    void create(Car car);

    void update(Car car);

    void delete(int id);

    List<Car> getUserCars(User user);
}
