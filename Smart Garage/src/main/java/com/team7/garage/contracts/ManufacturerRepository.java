package com.team7.garage.contracts;

import com.team7.garage.models.Manufacturer;

import java.util.List;

public interface ManufacturerRepository {
    List<Manufacturer> getAll();

    Manufacturer getById(int id);

    Manufacturer getByName(String name);

    Manufacturer getByNameOrCreate(String name);

    void create(Manufacturer manufacturer);

    void update(Manufacturer manufacturer);

    void delete(int id);
}
