package com.team7.garage.contracts;

import com.team7.garage.models.Car;
import com.team7.garage.models.User;

import java.util.List;
import java.util.Set;

public interface UserRepository {
    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    List<User> search(String email,
                      String firstName,
                      String lastName,
                      String phone);

    void create(User user);

    User getByToken(String token);

    void update(User user);

    void delete(int id);

}
