package com.team7.garage.contracts;

import com.team7.garage.models.Manufacturer;
import com.team7.garage.models.Model;

import java.util.List;

public interface ModelRepository {
    List<Model> getAll();

    Model getById(int id);

    Model getByName(String name);

    Model getByNameOrCreate(String name, Manufacturer manufacturer);

    void create(Model model);

    void update(Model model);

    void delete(int id);
}
