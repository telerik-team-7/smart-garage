package com.team7.garage.contracts;

import com.team7.garage.models.Visit;

import java.util.List;

public interface VisitRepository {
    List<Visit> getAll();

    List<Visit> getByCarId(int carId);

    List<Visit> getByProcedureId(int procedureId);

    List<Visit> getByUserId(int userId);

    Visit getById(int id);

    void create(Visit visit);

    void update(Visit visit);

    void delete(int id);

}
