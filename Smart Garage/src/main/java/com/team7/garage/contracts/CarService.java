package com.team7.garage.contracts;

import com.team7.garage.models.Car;
import com.team7.garage.models.User;

import java.util.List;

public interface CarService {
    List<Car> getAll(User user);

    Car getById(int id, User user);

    List<Car> filter(String vin,
                     String regNumber,
                     String model,
                     String manufacturer,
                     String userFirstName,
                     String userLastName,
                     String userEmail,
                     User user);
    void create(Car car, User user);

    void update(Car car, User user);

    void delete(int id, User user);

    List<Car> getUserCars(User user);
}
