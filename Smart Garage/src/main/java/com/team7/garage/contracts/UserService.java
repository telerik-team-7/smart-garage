package com.team7.garage.contracts;

import com.team7.garage.models.Car;
import com.team7.garage.models.User;

import java.util.List;
import java.util.Set;

public interface UserService {
    List<User> getAll(User user);

    User getById(int id,User userAuth);

    User getByEmail(String email);

    List<User> search(String email,
                      String firstName,
                      String lastName,
                      String phone,
                      User user);

    void create(User user);

    void update(User user, User authUser);

    void delete(int id, User userAuth);

    void updateResetPasswordToken(String token,String email);

    User getByToken(String token,String email, User userAuth);

    void updatePassword(String newPassword,User user);

    User getToken(String resetPasswordToken);


}
