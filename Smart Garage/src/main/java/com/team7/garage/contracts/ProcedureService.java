package com.team7.garage.contracts;

import com.team7.garage.models.Procedure;
import com.team7.garage.models.User;

import java.util.List;

public interface ProcedureService {
    List<Procedure> getAll(User user);

    Procedure getById(int id, User user);

    Procedure getByName(String name, User user);

    void create(Procedure procedure, User user);

    void update(Procedure procedure, User user);

    void delete(int id, User user);

    List<Procedure> getForUser(User user, Integer carId, String date, String beforeDate, String afterDate, User caller);

    List<Procedure> filter(String name, Double price, Double minPrice, Double maxPrice, User user);
}
