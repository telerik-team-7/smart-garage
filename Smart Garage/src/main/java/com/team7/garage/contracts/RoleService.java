package com.team7.garage.contracts;

import com.team7.garage.models.Role;
import com.team7.garage.models.User;

import java.util.List;
public interface RoleService {

    List<Role> getAll();

    Role getEmployeeRole();

    void create(Role role, User userAuth);

    void update(Role role, User userAuth);

    void delete(int id, User userAuth);
}
