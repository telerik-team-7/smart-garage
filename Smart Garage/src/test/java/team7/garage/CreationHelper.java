package team7.garage;

import com.team7.garage.models.*;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Set;

public class CreationHelper {

    public static Manufacturer createMockManufacturer() {
        var mockManufacturer = new Manufacturer();

        mockManufacturer.setId(1);
        mockManufacturer.setName("mockManufacturer");

        return mockManufacturer;
    }

    public static Model createMockModel() {
        var mockModel = new Model();

        mockModel.setId(1);
        mockModel.setName("mockModel");
        mockModel.setManufacturer(createMockManufacturer());

        return mockModel;
    }

    public static Role createMockUserRole() {
        var mockRole = new Role();

        mockRole.setId(1);
        mockRole.setName("User");

        return mockRole;
    }

    public static Role createMockEmployeeRole() {
        var mockRole = new Role();

        mockRole.setId(2);
        mockRole.setName("Employee");

        return mockRole;
    }
    public static Role createMockAdminRole() {
        var mockRole = new Role();

        mockRole.setId(2);
        mockRole.setName("Admin");

        return mockRole;
    }

    public static User createMockUser() {
        var mockUser = new User();

        mockUser.setId(1);
        mockUser.setFirstName("Mock");
        mockUser.setLastName("Customer");
        mockUser.setEmail("mock@customer.com");
        mockUser.setPhone("0888888888");
        mockUser.setPassword("pass123");
        mockUser.setToken("token");
        mockUser.setRoles(Set.of(createMockUserRole()));

        return mockUser;
    }

    public static User createMockEmployee() {
        var mockEmployee = new User();

        mockEmployee.setId(2);
        mockEmployee.setFirstName("Mock");
        mockEmployee.setLastName("Employee");
        mockEmployee.setEmail("mock@employee.com");
        mockEmployee.setPhone("0899999999");
        mockEmployee.setPassword("123pass");
        mockEmployee.setRoles(Set.of(createMockEmployeeRole()));

        return mockEmployee;
    }

    public static User createMockAdmin() {
        var mockEmployee = new User();

        mockEmployee.setId(2);
        mockEmployee.setFirstName("Mock");
        mockEmployee.setLastName("Employee");
        mockEmployee.setEmail("mock@employee.com");
        mockEmployee.setPhone("0899999999");
        mockEmployee.setPassword("123pass");
        mockEmployee.setRoles(Set.of(createMockAdminRole()));

        return mockEmployee;
    }

    public static Car createMockCar() {
        var mockCar = new Car();

        mockCar.setId(1);
        mockCar.setModel(createMockModel());
        mockCar.setUser(createMockUser());
        mockCar.setVin("123456789ABCDEFGH");
        mockCar.setRegNumber("ET2828AG");

        return mockCar;
    }

    public static Procedure createMockProcedure() {
        var mockProcedure = new Procedure();

        mockProcedure.setId(1);
        mockProcedure.setName("mockProcedure");
        mockProcedure.setPrice(10.00);

        return mockProcedure;
    }

    public static Visit createMockVisit() {
        var mockVisit = new Visit();

        mockVisit.setId(1);
        mockVisit.setCar(createMockCar());
        mockVisit.setProcedures(Set.of(createMockProcedure()));
        mockVisit.setStartDate(LocalDate.now());
        mockVisit.setEndDate(LocalDate.now().plus(1, ChronoUnit.DAYS));

        return mockVisit;
    }


}
