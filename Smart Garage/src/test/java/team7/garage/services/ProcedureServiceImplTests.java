package team7.garage.services;

import com.team7.garage.contracts.ProcedureRepository;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.services.ProcedureServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.text.DateFormatter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.mockito.Mockito.*;
import static team7.garage.CreationHelper.*;

@ExtendWith(MockitoExtension.class)
public class ProcedureServiceImplTests {

    @Mock
    ProcedureRepository procedureRepository;

    @InjectMocks
    ProcedureServiceImpl procedureService;

    @Test
    public void getAll_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.getAll(mockUser));
    }

    @Test
    public void getAll_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        when(procedureRepository.getAll())
                .thenReturn(List.of(createMockProcedure()));

        procedureService.getAll(mockUser);

        verify(procedureRepository, times(1)).getAll();
    }

    @Test
    public void getById_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.getById(1, mockUser));
    }

    @Test
    public void getById_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        when(procedureRepository.getById(1))
                .thenReturn(createMockProcedure());

        procedureService.getById(1, mockUser);

        verify(procedureRepository, times(1)).getById(1);
    }

    @Test
    public void getByName_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.getByName("mockProcedure", mockUser));
    }

    @Test
    public void getByName_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        when(procedureRepository.getByName("mockProcedure"))
                .thenReturn(createMockProcedure());

        procedureService.getByName("mockProcedure", mockUser);

        verify(procedureRepository, times(1)).getByName("mockProcedure");
    }

    @Test
    public void getForUser_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();
        var mockCaller = createMockUser();
        mockCaller.setEmail("mock@caller.com");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.getForUser(mockUser,
                        1,
                        "2020-01-01",
                        "2020-01-01",
                        "2020-01-01",
                        mockCaller));
    }

    @Test
    public void getForUser_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockUser();
        var mockEmployee = createMockAdmin();

        var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        when(procedureRepository.getForUser(mockUser,
                1,
                LocalDate.parse("01/01/2021", formatter),
                LocalDate.parse("01/01/2021", formatter),
                LocalDate.parse("01/01/2021", formatter)))
                .thenReturn(List.of(createMockProcedure()));

        procedureService.getForUser(mockUser,
                1,
                "01/01/2021",
                "01/01/2021",
                "01/01/2021",
                mockEmployee);

        verify(procedureRepository, times(1)).getForUser(mockUser,
                1,
                LocalDate.parse("01/01/2021", formatter),
                LocalDate.parse("01/01/2021", formatter),
                LocalDate.parse("01/01/2021", formatter));
    }

    @Test
    public void filter_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.filter(null,
                        null,
                        null,
                        null,
                        mockUser));
    }

    @Test
    public void filter_should_callRepository_whenUserIsEmployee() {
        var mockEmployee = createMockAdmin();

        when(procedureRepository.filter(anyString(),
                anyDouble(),
                anyDouble(),
                anyDouble())).thenReturn(List.of(createMockProcedure()));

        procedureService.filter("name",
                12.2,
                11.11,
                22.22,
                mockEmployee);

        verify(procedureRepository, times(1)).filter("name",
                12.2,
                11.11,
                22.22);
    }

    @Test
    public void create_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.create(createMockProcedure(), mockUser));
    }

    @Test
    public void create_shouldThrow_whenProcedureWithSameNameExists() {
        var mockUser = createMockAdmin();

        when(procedureRepository.getByName(anyString()))
                .thenReturn(createMockProcedure());

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> procedureService.create(createMockProcedure(), mockUser));
    }

    @Test
    public void create_shouldCallRepo_whenUserIsEmployeeAndProcedureIsUnique() {
        var mockUser = createMockAdmin();
        var mockProcedure = createMockProcedure();

        when(procedureRepository.getByName(anyString()))
                .thenThrow(EntityNotFoundException.class);

        procedureService.create(mockProcedure, mockUser);

        verify(procedureRepository, times(1)).create(mockProcedure);
    }

    @Test
    public void update_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.update(createMockProcedure(), mockUser));
    }

    @Test
    public void update_shouldThrow_whenProcedureWithSameNameExists() {
        var mockUser = createMockAdmin();
        var mockProcedure = createMockProcedure();
        mockProcedure.setId(2);

        when(procedureRepository.getByName(anyString()))
                .thenReturn(mockProcedure);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> procedureService.update(createMockProcedure(), mockUser));
    }

    @Test
    public void update_shouldCallRepo_whenUserIsEmployeeAndProcedureIsUnique() {
        var mockUser = createMockAdmin();
        var mockProcedure = createMockProcedure();

        when(procedureRepository.getByName(anyString()))
                .thenThrow(EntityNotFoundException.class);

        procedureService.update(mockProcedure, mockUser);

        verify(procedureRepository, times(1)).update(mockProcedure);
    }

    @Test
    public void delete_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> procedureService.delete(1, mockUser));
    }

    @Test
    public void delete_shouldCallRepo_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        procedureService.delete(1, mockUser);

        verify(procedureRepository, times(1)).delete(1);
    }
}
