package team7.garage.services;

import com.team7.garage.contracts.CarRepository;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.services.CarServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static team7.garage.CreationHelper.*;

@ExtendWith(MockitoExtension.class)
public class CarServiceImplTests {

    @Mock
    CarRepository carRepository;

    @InjectMocks
    CarServiceImpl carService;

    @Test
    public void getAll_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> carService.getAll(mockUser));
    }

    @Test
    public void getAll_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        when(carRepository.getAll())
                .thenReturn(List.of(createMockCar()));

        carService.getAll(mockUser);

        verify(carRepository, times(1)).getAll();
    }

    @Test
    public void getById_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> carService.getById(1, mockUser));
    }

    @Test
    public void getById_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        when(carRepository.getById(1))
                .thenReturn(createMockCar());

        carService.getById(1, mockUser);

        verify(carRepository, times(1)).getById(1);
    }

    @Test
    public void filter_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> carService.filter(null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        mockUser));
    }

    @Test
    public void filter_should_callRepository_whenUserIsEmployee() {
        var mockEmployee = createMockAdmin();

        when(carRepository.filter(anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString())).thenReturn(List.of(createMockCar()));

        carService.filter("vin",
                "reg",
                "model",
                "manufacturer",
                "firstName",
                "lastName",
                "email",
                mockEmployee);

        verify(carRepository, times(1)).filter("vin",
                "reg",
                "model",
                "manufacturer",
                "firstName",
                "lastName",
                "email");
    }

    @Test
    public void create_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> carService.create(createMockCar(), mockUser));
    }

    /*@Test
    public void create_shouldThrow_whenDuplicateExists() {
        var mockUser = createMockEmployee();

        when(carRepository.getByRegistration(anyString()))
                .thenReturn(createMockCar());


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> carService.create(createMockCar(), mockUser));
    }*/

    @Test
    public void create_shouldCallRepo_whenUserIsEmployeeAndCarIsUnique() {
        var mockUser = createMockAdmin();
        var mockCar = createMockCar();

        when(carRepository.getByRegistration(anyString()))
                .thenThrow(EntityNotFoundException.class);

        when(carRepository.getByVIN(anyString()))
                .thenThrow(EntityNotFoundException.class);

        carService.create(mockCar, mockUser);

        verify(carRepository, times(1)).create(mockCar);
    }

    @Test
    public void update_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> carService.update(createMockCar(), mockUser));
    }

    @Test
    public void update_shouldThrow_whenDuplicateCarExists() {
        var mockUser = createMockAdmin();
        var mockCar = createMockCar();
        mockCar.setId(2);

        when(carRepository.getByRegistration(anyString()))
                .thenReturn(mockCar);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> carService.update(createMockCar(), mockUser));
    }

    @Test
    public void update_shouldCallRepo_whenUserIsEmployeeAndCarIsUnique() {
        var mockUser = createMockAdmin();
        var mockCar = createMockCar();

        when(carRepository.getByRegistration(anyString()))
                .thenThrow(EntityNotFoundException.class);

        carService.update(mockCar, mockUser);

        verify(carRepository, times(1)).update(mockCar);
    }

    @Test
    public void delete_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> carService.delete(1, mockUser));
    }

    @Test
    public void delete_shouldCallRepo_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        carService.delete(1, mockUser);

        verify(carRepository, times(1)).delete(1);
    }
}
