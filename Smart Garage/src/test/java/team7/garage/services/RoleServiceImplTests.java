package team7.garage.services;

import com.team7.garage.contracts.RoleRepository;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.UnauthorizedOperationException;

import com.team7.garage.services.RoleServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static team7.garage.CreationHelper.*;
import static team7.garage.CreationHelper.createMockAdmin;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    public void getAll_should_callRepository_whenGivenCorrectData() {
        var mockUser = createMockAdmin();

        when(roleRepository.getAll())
                .thenReturn(List.of(createMockUserRole()));

        roleService.getAll();

        verify(roleRepository, times(1)).getAll();
    }


    @Test
    public void create_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> roleService.create(createMockUserRole(), mockUser));
    }


    @Test
    public void create_shouldCallRepo_whenUserIsEmployeeAndRoleIsUnique() {
        var mockUser = createMockAdmin();
        var mockRole = createMockUserRole();

        when(roleRepository.getByName(anyString()))
                .thenThrow(EntityNotFoundException.class);


        roleService.create(mockRole, mockUser);

        verify(roleRepository, times(1)).create(mockRole);
    }

    @Test
    public void update_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> roleService.update(createMockUserRole(), mockUser));
    }

    @Test
    public void delete_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> roleService.delete(1, mockUser));
    }

    @Test
    public void delete_shouldCallRepo_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        roleService.delete(1, mockUser);

        verify(roleRepository, times(1)).delete(1);
    }
}
