package team7.garage.services;

import com.team7.garage.contracts.UserRepository;
import com.team7.garage.exceptions.DuplicateEntityException;
import com.team7.garage.exceptions.EntityNotFoundException;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.services.UserServiceImpl;
import com.team7.garage.utils.EmailSender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import java.util.List;

import static org.mockito.Mockito.*;
import static team7.garage.CreationHelper.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @Mock
    EmailSender emailSender;
    @InjectMocks
    UserServiceImpl userService;


    @Test
    public void getAll_should_callRepository() {
        var mockUser = createMockAdmin();

        when(userRepository.getAll())
                .thenReturn(List.of(createMockUser()));

        userService.getAll(mockUser);

        verify(userRepository, times(1)).getAll();
    }


    @Test
    public void getById_should_callRepository() {

        var mockUser = createMockAdmin();
        when(userRepository.getById(1))
                .thenReturn(createMockUser());

        userService.getById(1, mockUser);

        verify(userRepository, times(1)).getById(1);
    }

    @Test
    public void getByEmail_should_callRepository() {
        var mockUser = createMockEmployee();

        when(userRepository.getByEmail(anyString()))
                .thenReturn(createMockUser());

        userService.getByEmail(anyString());

        verify(userRepository, times(1))
                .getByEmail(anyString());
    }
   /*
    @Test
    public void search_shouldCallRepo_whenGivenCorrectData() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.search("mock",
                        "mock",
                        "mock",
                        "mock",
                        "mock",
                        "mock",
                        "mock", "mock"));


        when(userService.search(anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString(),
                anyString())).thenReturn(List.of(createMockUser()));

        userService.search("mock",
                "mock",
                "mock",
                "mock",
                "mock",
                "mock",
                "mock",
                "mock");

        verify(userRepository, times(1)).search("mock",
                "mock",
                "mock",
                "mock",
                "mock",
                "mock",
                "mock",
                "mock");
    }*/

    @Test
    public void updateResetPasswordToken_shouldThrow_whenUserDoesNotExist() {
        var mockUser = createMockUser();

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.updateResetPasswordToken("token","email"));
    }

    @Test
    public void updateResetPasswordToken_shouldCallRepo_whenGivenValidData() {
        var mockUser = createMockUser();

        when(userRepository.getByEmail(anyString()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(mockUser));
    }


    @Test
    public void create_shouldCallRepo_whenUserEmailIsUnique() {
        var mockUser = createMockUser();
        mockUser.setEmail("newEmail@mail.com");

        when(userRepository.getByEmail(anyString()))
                .thenThrow(EntityNotFoundException.class);

        userService.create(mockUser);

        verify(userRepository, times(1)).create(mockUser);
    }

    @Test
    public void update_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();
        mockUser.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.update(mockUser, createMockUser()));
    }

    @Test
    public void update_shouldThrow_whenUserWithSameEmailExists() {
        var mockUser = createMockUser();
        var mockEmployee = createMockEmployee();
        mockUser.setId(2);

        when(userRepository.getByEmail(anyString()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.update(createMockUser(), createMockAdmin()));
    }

    @Test
    public void update_shouldCallRepo_whenUserIsEmployeeAndUserIsUnique() {
        var mockUser = createMockUser();
        var mockEmployee = createMockEmployee();

        when(userRepository.getByEmail(anyString()))
                .thenThrow(EntityNotFoundException.class);

        userService.update(mockUser, createMockAdmin());

        verify(userRepository, times(1)).update(mockUser);
    }

    @Test
    public void delete_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();
        mockUser.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.delete(1, mockUser));
    }

    @Test
    public void delete_shouldCallRepo_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        userService.delete(1, mockUser);

        verify(userRepository, times(1)).delete(1);
    }

    @Test
    public void updatePassword_shouldCallRepo_whenGivenValidData() {
        var mockUser = createMockUser();
        userService.updatePassword(anyString(), mockUser);

        verify(userRepository, times(1)).update(mockUser);
    }


    @Test
    public void getByToken_shouldThrow_whenUserIsNotAdmin() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.getByToken("token", "mock@customer.com", mockUser));
    }

    @Test
    public void getByToken_shouldCallRepo_whenUserIsAdmin() {
        var mockUser = createMockAdmin();
        userService.getByToken("token", "mock@customer.com", mockUser);

        verify(userRepository, times(1)).getByToken("token");
    }

    @Test
    public void updatePassword_shouldCallRepo_givenCorrectData() {
        var mockUser = createMockUser();
        userService.updatePassword("token", mockUser);

        verify(userRepository, times(1)).update(mockUser);
    }


}
