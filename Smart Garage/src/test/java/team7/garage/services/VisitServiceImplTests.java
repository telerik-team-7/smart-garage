package team7.garage.services;

import com.team7.garage.contracts.VisitRepository;
import com.team7.garage.exceptions.UnauthorizedOperationException;
import com.team7.garage.services.VisitServiceImpl;
import com.team7.garage.utils.EmailSender;
import com.team7.garage.utils.ModelMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;
import static team7.garage.CreationHelper.*;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    private static final String CURRENCY = "BGN";
    @Mock
    VisitRepository visitRepository;

    @Mock
    ModelMapper modelMapper;

    @Mock
    EmailSender emailSender;

    @InjectMocks
    VisitServiceImpl visitService;

    @Test
    public void getAll_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.getAll(mockUser, CURRENCY));
    }

    @Test
    public void getAll_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        when(visitRepository.getAll())
                .thenReturn(List.of(createMockVisit()));

        visitService.getAll(mockUser, CURRENCY);

        verify(visitRepository, times(1)).getAll();
    }

    @Test
    public void getById_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.getById(1,"BGN", mockUser));
    }

    @Test
    public void getById_should_callRepository_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        when(visitRepository.getById(anyInt()))
                .thenReturn(createMockVisit());

        visitService.getById(1, "BGN", mockUser);

        verify(visitRepository, times(1)).getById(1);
    }

    @Test
    public void create_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.create(createMockVisit(), mockUser));
    }

    @Test
    public void create_shouldCallRepo_whenUserIsEmployee() {
        var mockUser = createMockAdmin();
        var mockVisit = createMockVisit();

        visitService.create(mockVisit, mockUser);

        verify(visitRepository, times(1)).create(mockVisit);
    }

    @Test
    public void update_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.update(createMockVisit(), mockUser));
    }

    @Test
    public void update_shouldCallRepo_whenUserIsEmployee() {
        var mockUser = createMockAdmin();
        var mockVisit = createMockVisit();

        visitService.update(mockVisit, mockUser);

        verify(visitRepository, times(1)).update(mockVisit);
    }

    @Test
    public void delete_shouldThrow_whenUserIsNotEmployee() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.delete(1, mockUser));
    }

    @Test
    public void delete_shouldCallRepo_whenUserIsEmployee() {
        var mockUser = createMockAdmin();

        visitService.delete(1, mockUser);

        verify(visitRepository, times(1)).delete(1);
    }

}
