create schema smart_garage collate latin1_swedish_ci;

create table manufacturers
(
    manufacturer_id int auto_increment
        primary key,
    name varchar(30) null
);

create table models
(
    model_id int auto_increment
        primary key,
    name varchar(30) null,
    manufacturer_id int null,
    constraint models_manufacturers_manufacturer_id_fk
        foreign key (manufacturer_id) references manufacturers (manufacturer_id)
);

create table procedures
(
    procedure_id int auto_increment
        primary key,
    name varchar(30) null,
    price double null
);

create table roles
(
    role_id int auto_increment
        primary key,
    name text null
);

create table users
(
        user_id              int auto_increment
        primary key,
    first_name           varchar(20) null,
    last_name            varchar(20) null,
    phone                varchar(10) null,
    email                varchar(30) null,
    password             text        null,
    password_reset_token varchar(30) null

);

create table cars
(
    car_id int auto_increment
        primary key,
    vin varchar(17) not null,
    reg_number varchar(8) null,
    model_id int null,
    user_id int null,
    constraint cars_vin_uindex
        unique (vin),
    constraint cars_models_model_id_fk
        foreign key (model_id) references models (model_id),
    constraint cars_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table cars_users
(
    cars_id int not null
        primary key,
    user_id int not null,
    constraint cars_users_cars_car_id_fk
        foreign key (cars_id) references cars (car_id),
    constraint cars_users_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table users_roles
(
    user_id int null,
    role_id int null,
    constraint users_roles_roles_id_fk
        foreign key (role_id) references roles (role_id),
    constraint users_roles_users_id_fk
        foreign key (user_id) references users (user_id)
);

create table visits
(
    visit_id int auto_increment
        primary key,
    car_id int null,
    start_date date null,
    end_date date null,
    constraint visits_cars_car_id_fk
        foreign key (car_id) references cars (car_id)
);

create table visits_procedures
(
    visit_id int null,
    procedure_id int null,
    constraint visits_services_services_service_id_fk
        foreign key (procedure_id) references procedures (procedure_id),
    constraint visits_services_visits_visit_id_fk
        foreign key (visit_id) references visits (visit_id)
);

