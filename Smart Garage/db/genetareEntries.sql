insert into roles (role_id, name)
values (1, 'User');

insert into roles (role_id, name)
values (2, 'Admin');

insert into users (user_id, first_name, last_name, phone, email, password,password_reset_token)
values (1, 'John', 'Shepard', '0898888888',
        'iShouldGo@Citadel.com', 'hamster', null);


insert into users (user_id, first_name, last_name, phone, email, password,password_reset_token)
values (2, 'Garrus', 'Vakarian','0888888888',
        'iShootBottles@Citadel.com', 'calibrations',null);

insert into users (user_id, first_name, last_name, phone, email, password,password_reset_token)
values (3, 'Tali', 'Zorah','0899999999',
        'boshtet@Citadel.com', 'itsastraw',null);

insert into users_roles (user_id, role_id)
values (1, 1);

insert into users_roles (user_id, role_id)
values (2, 1);

insert into users_roles (user_id, role_id)
values (2, 2);

insert into users_roles (user_id, role_id)
values (3, 1);

insert into procedures (procedure_id, name, price)
values (1, 'Oil Change', 50);

insert into procedures (procedure_id, name, price)
values (2, 'Filters Change', 70);

insert into procedures (procedure_id, name, price)
values (3, 'Tyres Change', 20);

insert into procedures (procedure_id, name, price)
values (4, 'Check-Up', 80);

insert into manufacturers (manufacturer_id, name)
values (1, 'Opel');

insert into models (model_id, name, manufacturer_id)
values (1, 'Astra', 1);

insert into cars (car_id, vin, reg_number, model_id, user_id)
values (1, '12345678910111213', 'ET8989AC',
        1, 2);

insert into visits (visit_id, car_id, start_date, end_date)
values (1, 1, '2021-01-12', '2021-01-22');

insert into visits_procedures (visit_id, procedure_id)
values (1, 1);

insert into visits_procedures (visit_id, procedure_id)
values (1, 2);